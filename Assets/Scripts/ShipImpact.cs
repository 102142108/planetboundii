﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipImpact : MonoBehaviour
{
    GameObject GameManager;
    Scoring scoring;
    // Start is called before the first frame update
    void Start()
    {
        GameManager = GameObject.Find("GameManager");
        scoring = GameManager.GetComponent<Scoring>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Asteroid")
        {
            Debug.Log("We hit an Asteroid");
            scoring.health -= 1;
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.tag == "Obstacle")
        {
            Debug.Log("We hit an Obstacle");
            scoring.health -= 1;
            //Destroy(collision.gameObject);
        }
    }
}

