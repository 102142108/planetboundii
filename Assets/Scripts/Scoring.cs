﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Scoring : MonoBehaviour
{
    public int Score;
    public Text timeText;
    public Text scoreText;
    public Text healthText;
    float time;
    public int health;
    public GameObject loseCanvas;
    public string sceneName;
    public GameObject gun;
    // Start is called before the first frame update
    void Start()
    {
    }

    void DisplayTime(float timeToDisplay)
    {
        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);
        float milliSeconds = (timeToDisplay % 1) * 1000;

        timeText.text = string.Format("{0:00}:{1:00}:{2:000}", minutes, seconds, milliSeconds);
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(Time.deltaTime);
        time += Time.deltaTime;
        DisplayTime(time);
        scoreText.text = Score.ToString();
        if (health == 3)
        {
            healthText.text = "O O O";
        }else if(health == 2)
        {
            healthText.text = "O O";
        }
        else if (health == 1)
        {
            healthText.text = "O";
        }
        else if (health <= 0)
        {
            gun.SetActive(false);
            //Insert some end of game thing.
            loseCanvas.SetActive(true);
            if (Input.GetKey(KeyCode.Space))
            {
                LoadScene();
            }
        }
    }
    public void LoadScene()
    {
        if (sceneName == null)
            Debug.Log("<color=orange>" + gameObject.name + ": No Scene Name Was given for LoadScene function!</color>");
        SceneManager.LoadScene(sceneName); //load a scene
    }
}
