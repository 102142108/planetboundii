﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateFields : MonoBehaviour
{
    GameObject GameManager;
    Scoring scoring;
    public Text scoreText;
    public Text timeText;
    // Start is called before the first frame update
    void Start()
    {
        GameManager = GameObject.Find("GameManager");
        scoring = GameManager.GetComponent<Scoring>();
        String score = scoring.scoreText.text;
        String time = scoring.timeText.text;
        scoreText.text = score;
        timeText.text = time;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
