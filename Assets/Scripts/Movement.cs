﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Movement : MonoBehaviour
{
    public float speed;
    public float speedr;
    private Rigidbody rigidbody;
    public GameObject canvas;
    public string sceneName;
    Vector3 movement;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        movement = rigidbody.transform.position;
        if (movement.x > -850)
        {
            movement.x = movement.x - (speed / 10);
        }
        if (Input.GetKey(KeyCode.A) && movement.z > -40)
        {
            movement.z = movement.z - (speedr / 10);
        }
        if (Input.GetKey(KeyCode.D) && movement.z < 40)
        {
            movement.z = movement.z + (speedr / 10);
        }
        rigidbody.position = movement;
    }
    void Update()
    {
        if (movement.x < -800)
        {
            Debug.Log("End of level" + "Load next Scene");
            if (canvas.active == false)
            {
                canvas.SetActive(true);
            }
            if (Input.GetKey(KeyCode.Space))
            {
                LoadScene();
            }
        }
    }

    public void LoadScene()
    {
        if (sceneName == null)
            Debug.Log("<color=orange>" + gameObject.name + ": No Scene Name Was given for LoadScene function!</color>");
        SceneManager.LoadScene(sceneName); //load a scene
    }
}
